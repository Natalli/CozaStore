const express = require('express');
const expressNunjucks = require('express-nunjucks');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const jsonfile = require('jsonfile');
const PORT = 3000;

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
const njk = expressNunjucks(app);

const shopFile = 'front/src/data/shop.json';

const usersFile = 'front/src/data/users.json';

app.get('/', function (req, res) {
    res.send(200, 'Hello');
});

app.post('/set-shopping-data', function (req, res) {
    const params = req.body;
    jsonfile.writeFile(shopFile, params, {spaces: 2}, function (err, obj) {
        res.send(200, params);
    });
    res.send(200, params);
});

app.post('/get-user-data', function (req, res) {
    let responseData = {errorText: '', userName: ''};
    const userDataToCheck = req.body;

    jsonfile.readFile(usersFile, function (err, obj) {
        const usersArr = obj.users;
        for (let i = 0; i < usersArr.length; i++) {

            if (usersArr[i].email === userDataToCheck.userEmail && usersArr[i].password === userDataToCheck.userPassword) {
                responseData.userName = usersArr[i].name;
                responseData.errorText = '';
                break;
            } else {
                responseData.errorText = 'A user with this data does not exist';
            }
        }
        res.status(200).send(responseData);
    });

});

app.listen(PORT);