$(document).ready(function () {
    getCategories();
    getProductCategories();
});

function getProductCategories() {

    $.getJSON("data/categories.json", function (response) {
        let parent = $('#category');
        let categories = response.categories;
        parent.append(
            $('<button/>', {class: 'category-item stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5 cap how-active1'}).attr('data-filter', '*').text('All')
        );
        categories.forEach((category, index) => {
            parent.append(
                $('<button/>', {class: `category-item stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5 cap ${category.categoryName}`}).attr('data-filter', '.' + category.categoryName).text(category.categoryName)
            );
            $.getJSON("data/products.json", function (response) {
                response.products.filter(function (product) {

                    let productColorString;
                    if (product.color) {
                        let productColor = product.color;
                        productColorString = productColor.join(' ');
                    }
                    (product.categoryId === index + 1) && $('<div/>', {
                        class: `product-item col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item all ${category.categoryName} ${productColorString} allColors`,
                        'data-price': product.cost,
                        html: `<div class="block-2">
                                <div class="block2-pic hov-img0">
                                 <img src=${product.img[0]}>
                                 <a class="viewProduct block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 pointer" data-category=${category.categoryName} data-id=${product.art}>View</a>
                                </div>
                                <div class="block2-txt flex-w flex-t p-t-14">
                             <div class="block2-txt-child1 flex-col-l">
                              <span class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6 pointer">
                                        ${product.prod_name}
                                    </span>
                                    <span class="stext-105 cl3">
										$ ${product.cost}
									</span>
                             </div>
                            </div>
                               </div>`
                    }).appendTo($('#productsWrapper'));
                });
            });
        });
        $('.category-item').on('click', activeItemCategory);
    });
}
function activeItemCategory() {
    $('#category .category-item').removeClass('how-active1');
    $(this).addClass('how-active1');
}

