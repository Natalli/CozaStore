'use strict';

$(document).ready(function () {

    let items = JSON.parse(localStorage.getItem("products"));
    (items) ? ($('.icon-header-item').attr('data-notify', items.length)) :
        ($('.icon-header-item').attr('data-notify', 0));

    let isValidShip = false;

    $('.sec-product-detail .js-addcart-detail').click(addToCartHandler);

    $('#modal .js-addcart-detail').click(addToCartInModalHandler);


    $('.js-show-cart').on('click', function () {
        checkCart();
    });


    $('select.sizeSelect').on('change', function () {
        $('.sizeArea > .error-msg').remove();
    });
    $('select.colorSelect').on('change', function () {
        $('.colorArea > .error-msg').remove();
    });


//-----------------------Main Cart-----------------------------------------

    (location.pathname === '/shoping-cart.html') && cartDataFromLocaleStorage();

    $(document).on('click', '.how-itemcart1', function (e) {
        let index = $(e.target).parent().parent().attr('data-index');
        let items = JSON.parse(localStorage.getItem("products"));

        items.splice(index, 1);

        let serialItems = JSON.stringify(items);
        localStorage.setItem("products", serialItems);

        cartDataFromLocaleStorage();

        $('.icon-header-item').attr('data-notify', items.length);
    });

    $(document).on('click', '.header-cart-item-img', function (e) {
        let index = $(e.target).parent().attr('data-index');
        let items = JSON.parse(localStorage.getItem("products"));

        items.splice(index, 1);

        let serialItems = JSON.stringify(items);
        localStorage.setItem("products", serialItems);

        checkCart();
        $('.icon-header-item').attr('data-notify', items.length);
    });

    function cartDataFromLocaleStorage() {
        $('.table-shopping-cart tbody .table_row').remove();
        let tableCart = $('.table-shopping-cart tbody');
        let items = JSON.parse(localStorage.getItem("products"));
        let totalPrice;

        if (items) {
            totalPrice = 0;
            items.forEach(function (item, index) {
                $('<tr/>', {
                    class: 'table_row',
                    'data-index': index,
                    html: `<td class="column-1"><div class="how-itemcart1"><img src=${item.productImagePath}></div></td>
               <td class="column-2">${item.name}</td>
               <td class="column-3">${item.price}</td>
               <td class="column-4">
                 <div class="wrap-num-product flex-w m-l-auto m-r-0">
                   
                    <input class="m-auto mtext-104 cl3 txt-center num-product" type="number" name="num-product${index + 1}" value=${parseInt(item.quantity)}>
                    
                 </div></td>
               <td class="column-5">$ ${parseFloat((item.price).slice(1)) * parseInt(item.quantity)}</td>`
                }).appendTo(tableCart);

                totalPrice += parseFloat((item.price).slice(1)) * parseInt(item.quantity);
            });
        } else {
            totalPrice = 0;
        }

        $('.totalPrice').html(`$ ${totalPrice.toFixed(2)}`);

        $('#buyBtn').on('click', function () {
            isValidShip = false;
            let selectCountry = $('.selectCountry').val();
            let selectState = $('.selectState').val();
            let zip = $('.zipCode').val();

            checkValidationField(selectCountry, selectState, zip);

            if (!isValidShip) {
                $.ajax({
                    url: "http://localhost:3000/set-shopping-data",
                    method: "POST",
                    data: {
                        country: selectCountry,
                        state: selectState,
                        zip: zip,
                        totalPrice: totalPrice,
                        shoppingList: items
                    }
                }).done(function (response) {
                    console.log(response);
                })
            }
        });

    }

//------------------------------------------------------------------
    function addToCartHandler() {
        let sizeAreaError = $('.sec-product-detail .sizeArea > .error-msg');
        let colorAreaError = $('.sec-product-detail .colorArea > .error-msg');

        clearErrorMsg(sizeAreaError, colorAreaError);

        let name, price, quantity, productImagePath;

        let sizeOption = $('.sec-product-detail select.sizeSelect');
        let colorOption = $('.sec-product-detail select.colorSelect');

        let selectSize = sizeOption.val();
        let selectColor = colorOption.val();

        if (selectSize === 'Choose an option' && !isDisabledOption(sizeOption)) {
            let parent = $('.sec-product-detail .sizeArea');
            errorMsg(parent);
            selectSize = false;
        }

        if (selectColor === 'Choose an option' && !isDisabledOption(colorOption)) {
            let parent = $('.sec-product-detail .colorArea');
            errorMsg(parent);
            selectSize = false;
        }

        if (selectSize && selectColor) {
            name = $('.sec-product-detail .productTitle').text();
            price = $('.sec-product-detail .productPrice').text();
            quantity = $('.sec-product-detail .num-product').val();
            productImagePath = $('.item-slick3').attr('data-thumb');

            setlocalStorage(name, price, quantity, productImagePath);
        }
    }

    function addToCartInModalHandler() {
        let sizeAreaError = $('#modal .sizeArea > .error-msg');
        let colorAreaError = $('#modal .colorArea > .error-msg');

        clearErrorMsg(sizeAreaError,colorAreaError);

        let name, price, quantity, productImagePath;

        let sizeOption = $('#modal select.sizeSelect');
        let colorOption = $('#modal select.colorSelect');

        let selectSize = sizeOption.val();
        let selectColor = colorOption.val();

        if (selectSize === 'Choose an option' && !isDisabledOption(sizeOption)) {
            let parent = $('#modal .sizeArea');
            errorMsg(parent);
            selectSize = false;
        }

        if (selectColor === 'Choose an option' && !isDisabledOption(colorOption)) {
            let parent = $('#modal .colorArea');
            errorMsg(parent);
            selectSize = false;
        }

        if (selectSize && selectColor) {
            name = $('#modal .productName').text();
            price = $('#modal .productCost').text();
            quantity = $('#modal .num-product').val();
            productImagePath = $('#modal .productImages img').attr('src');

            setlocalStorage(name, price, quantity, productImagePath);
        }
    }

    function clearErrorMsg(sizeAreaError, colorAreaError) {
        sizeAreaError.remove();
        colorAreaError.remove();
    }
    function errorMsg(parent) {
        $('<div/>', {
            class: 'error-msg size-204 text-center text-danger',
            html: `Choose the option`
        }).appendTo(parent);
    }

    function isDisabledOption(option) {
        return option.attr('disabled') === 'disabled';
    }

    function checkCart() {                                                                   //если пустой локал сторадж то в мини-корзину ничего не добавляется
        $('.header-cart-wrapitem').html('');
        let totalPrice;
        let items = JSON.parse(localStorage.getItem("products"));
        if (items) {
            let productPrice;
            totalPrice = 0;
            for (let i = 0; i < items.length; i++) {
                getItems(items[i].name, items[i].price, items[i].quantity, items[i].productImagePath, i);
                productPrice = parseFloat(items[i].price.slice(1)) * parseInt(items[i].quantity);
                totalPrice += productPrice;
            }
        } else {
            totalPrice = 0;
        }

        $('.header-cart-total').html(`Total: $${totalPrice.toFixed(2)}`);
    }

    function setlocalStorage(_name, _price, _quantity, _productImagePath) {                    //добавление товара в локал сторадж
        let obj = [];
        let items = JSON.parse(localStorage.getItem("products"));

        if (items) {
            $('.icon-header-item').attr('data-notify', items.length + 1);
            for (let i = 0; i < items.length; i++) {
                obj.push({
                    name: items[i].name,
                    price: items[i].price,
                    quantity: items[i].quantity,
                    productImagePath: items[i].productImagePath
                });
            }
        } else {
            $('.icon-header-item').attr('data-notify', 1);
        }

        obj.push({
            name: _name,
            price: _price,
            quantity: _quantity,
            productImagePath: _productImagePath
        });

        let serialObj = JSON.stringify(obj);
        localStorage.setItem("products", serialObj);


    }

    function getItems(_name, _price, _quantity, _productImagePath, index) {  // получить и записать в мини-корзину
        let list = $('.header-cart-wrapitem');
        $('<li/>', {
            class: 'header-cart-item flex-w flex-t m-b-12',
            'data-index': index,
            html: `<div class="header-cart-item-img"><img src=${_productImagePath} alt="IMG"></div>
               <div class="header-cart-item-txt p-t-8">
                <a href="#" class="header-cart-item-name m-b-18 hov-cl1 trans-04">${_name}</a>
                <span class="header-cart-item-info">${_quantity} x ${_price}</span>
               </div>`

        }).appendTo(list);


        // let li = document.createElement('li');
        // let div = document.createElement('div');
        // let a = document.createElement('a');
        // let span = document.createElement('span');
        // let name = _name;
        // let price = _price;
        // let quantity = _quantity;

        // a.href = '/';
        // a.classList.add('header-cart-item-name', 'm-b-18', 'hov-cl', 'trans-04');
        // div.classList.add('header-cart-item-txt', 'p-t-8');
        // li.classList.add('header-cart-item', 'flex-w', 'flex-t', 'm-b-12');
        // span.classList.add('header-cart-item-info');
        //
        // a.innerHTML = name;
        // span.innerHTML = quantity + " x " + price;

        // div.appendChild(a);
        // div.appendChild(span);
        // li.appendChild(div);
        // $(li).appendTo(list);

        let totalPrice = 0;
        for (let i = 3; i < list[0].children.length; i++) {                  // когда уберутся статичные элементы поставить i=0
            // let str = list[0].children[i].children[0].children[1].textContent;
            // let shift = 0;
            // while (true) {
            //     if (str[shift] !== ' ') {
            //         shift++;
            //     }
            //     else {
            //         break;
            //     }
            // }
            // let strQuantity = str.substring(0, shift);
            // let strPrice = str.substring(shift + 4);
            //
            // totalPrice += strQuantity * strPrice;
        }

        // let total = $('.header-cart-total');
        // let totalText = "Total: $" + totalPrice;
        // total.text(totalText);
    }

    function checkValidationField(selectCountry, selectState, zip) {
        let patternState = new RegExp(/^[A-Z][a-z]{1,}$/),
            patternZip = new RegExp(/^\d{5}$/);

        const resultState = patternState.exec(selectState);
        const resultZip = patternZip.exec(zip);
        checkForErrorShip({key: 'state', pattern: resultState, value: selectState});
        checkForErrorShip({key: 'zip', pattern: resultZip, value: zip});

        if (selectCountry === "Select a country...") {
            isValidShip = true;
            return showMessageShip('country');
        } else {
            $(`#shipForm select[name='country']`).parent().removeClass('error');
        }
    }

    function checkForErrorShip(obj) {
        if (!obj.value) {
            $(`#shipForm input[name=${obj.key}]`).parent().addClass('error');
            isValidShip = true;
            return showMessageShip(obj.key);
        } else {
            showMessageShip(obj.key);
            $(`#shipForm input[name=${obj.key}]`).parent().removeClass('error');
        }

        if (!obj.pattern) {
            $(`#shipForm input[name=${obj.key}]`).parent().addClass('error');
            isValidShip = true;
            return showMessageShip(obj.key);
        } else {
            $(`#shipForm input[name=${obj.key}]`).parent().removeClass('error');
        }
    }

    function showMessageShip(thisElem) {
        const element = $(`#shipForm input[name=${thisElem}], #shipForm select[name=${thisElem}]`);
        element.parent().removeClass('error');
        element.parent().addClass('error');
    }


});

