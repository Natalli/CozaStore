$(document).ready(function () {

    function LogInForm(userEmail, userPassword) {

        this.validateData = () => {

            clearErrorMsg();

            if (!userEmail || !userPassword) {

                showError('Fill in all the fields');

            } else if (!isValidEmail(userEmail) || !isValidPassword(userPassword)){

                showError('Enter the correct data');

            } else {

                $.ajax({
                    url: "http://localhost:3000/get-user-data",
                    method: "POST",
                    data: {userEmail: userEmail, userPassword: userPassword}

                }).done(function (response) {

                    (response.errorText) && showError(response.errorText);

                    if (response.userName) {
                        let parent = $('.wrap-icon-header');
                        $('#logInModal').modal('hide');
                        $('<div/>', {
                            class: 'user-active',
                            html: `Hello, ${response.userName}!`
                        }).appendTo(parent);

                        $('.loginBtnMenu').parent().hide();
                    }
                });
            }

            function showError(text) {
                let parent = $('#logInForm');
                $('<div/>', {
                    class: 'error-msg text-center',
                    html: text
                }).appendTo(parent);
            }

            function clearErrorMsg() {
                $('#logInForm > div.error-msg').remove();
            }

            function isValidEmail(email) {
                let emailPattern = /((\w+\.)*\w+)@((\w+\.)+\w+)/;
                return emailPattern.test(email);
            }

            function isValidPassword(password) {
                let pswPattern = /(\w{4})+/;
                return pswPattern.test(password);
            }
        };
    }

    $('.loginBtnMenu').on('click', function () {
        $('#inputEmail').val('');
        $('#inputPassword').val('');
    });

    $('#logInBtn').on('click', function () {

        let emailValue = $('#inputEmail').val();
        let passwordValue = $('#inputPassword').val();

        let newLogInForm = new LogInForm(emailValue, passwordValue);
        newLogInForm.validateData();

    });

});
