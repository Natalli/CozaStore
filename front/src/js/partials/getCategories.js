'use strict';

/*==================================================================
 [ Isotope ]*/
let $topeContainer = $('.isotope-grid');
let $filter = $('.filter-tope-group');
let $sortBy = $('.filter-col1');
let $filterByPrice = $('.filter-col2');
let $filterColor = $('.filter-col3');

function getComboFilter () {
    let categoryActive = $('#category .how-active1').text().toLowerCase();
    let productPriceMin = parseInt($('.filter-col2 .filter-link-active').attr('data-price-min')) || 0;
    let productPriceMax = parseInt($('.filter-col2 .filter-link-active').attr('data-price-max')) || 1000000000;
    let colorActive = ($('.filter-col3 .filter-link-active').text().toLowerCase()) || 'allColors';

    return ($(this).hasClass(categoryActive) &&
    $(this).hasClass(colorActive) &&
    (parseFloat($(this).attr('data-price')) >= productPriceMin) &&
    (parseFloat($(this).attr('data-price')) <= productPriceMax));
}

// filter items on button click
//----------------Was-------------------------------
// $filter.each(function () {
//     $filter.on('click', 'button', function (e) {
//         e.preventDefault();
//         let filterValue = $(this).attr('data-filter');
//         $topeContainer.isotope({filter: filterValue});
//         $('.filter-col3').find('.filter-link-active').removeClass('filter-link-active');
//     });
// });


$filter.each(function () {
    $filter.on('click', 'button', function (e) {
        e.preventDefault();
        // let filterValue = $(this).attr('data-filter');
        $topeContainer.isotope({filter: getComboFilter});
        $('.filter-col3').find('.filter-link-active').removeClass('filter-link-active');
    });
});

//-----------------------------------------------------

$sortBy.on('click', 'a', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('filter-link-active')) {
        $('.filter-col1').find('.filter-link-active').removeClass('filter-link-active');
        $(this).addClass('filter-link-active');
    }
    let sortValue = $(this).attr('data-sort-by');
    let acs = $(this).hasClass('acs');
    $topeContainer.isotope({
        sortBy: sortValue,
        sortAscending: acs
    });

});


//------------------------------------------------------------------------

$filterByPrice.on('click', '.filter-link', function (e) {
    e.preventDefault();
    // let productPriceMin = $(this).attr('data-price-min');
    // let productPriceMax = $(this).attr('data-price-max');
    // let categoryActive = $('#category .how-active1').text().toLowerCase();
    // let colorActive = $('.filter-col3 .filter-link-active').text().toLowerCase();


    if (!$(this).hasClass('filter-link-active')) {
        $('.filter-col2').find('.filter-link-active').removeClass('filter-link-active');
        $(this).addClass('filter-link-active');
    }

    $topeContainer.isotope({
        // filter: function () {
        //     let productPrice = parseFloat($(this).attr('data-price'));
        //     return (productPrice >= parseInt(productPriceMin) &&
        //     $(this).hasClass(categoryActive) &&
        //     $(this).hasClass(colorActive));
        //
        //     // return $(this).hasClass('black');
        // }
        filter: getComboFilter
    });

});
//-------------------------------------------------------------------

//--------------------------Was----------------------------------------

$filterColor.on('click', '.filter-link', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('filter-link-active')) {
        $('.filter-col3').find('.filter-link-active').removeClass('filter-link-active');
        $(this).addClass('filter-link-active');
    }
    // let activeCategory = $('#category .how-active1').text().toLowerCase();
    // let selectedColor = $(this).text().toLowerCase();
    // let selector = '.' + activeCategory;
    // // $topeContainer.isotope({filter: selector + '.' + selectedColor});
    $topeContainer.isotope({filter: getComboFilter});
});


//-------------------------------------------------------------------------

// init Isotope
$(window).on('load', function () {
    let $grid = $topeContainer.each(function () {
        $(this).isotope({
            itemSelector: '.isotope-item',
            layoutMode: 'fitRows',
            percentPosition: true,
            animationEngine: 'best-available',
            masonry: {
                columnWidth: '.isotope-item'
            },
            getSortData: {
                cost: '[data-price] parseFloat'
            }
        });
    });
    activeCategory($grid);
});

let isotopeButton = $('.filter-tope-group button');

$(isotopeButton).each(function () {
    $(this).on('click', function () {
        for (let i = 0; i < isotopeButton.length; i++) {
            $(isotopeButton[i]).removeClass('how-active1');
        }

        $(this).addClass('how-active1');

        // console.log($('.filter-col3 .filter-link.filter-link-active'));
    });


});

function getCategories() {
    $.getJSON("data/categories.json", function (response) {
        let parent = $('#categoriesBox');
        let categories = response.categories;
        console.log(response);
        categories.forEach((category) => {
            parent.append(
                $('<div/>', {class: 'col-md-6 col-xl-6 p-b-30 m-lr-auto'}).append(
                    $('<div/>', {class: 'block1 wrap-pic-w'}).append(
                        $('<img/>').attr('src', category.banner),
                        $('<a/>', {class: 'block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3 pointer'}).attr({
                            'data-id': category.categoryId,
                            goToInnerCategory
                        }).append(
                            $('<div/>', {class: 'block1-txt-child1 flex-col-l'}).append(
                                $('<span/>', {class: 'block1-name ltext-102 trans-04 p-b-8 cap'}).text(category.categoryName),
                                $('<span/>', {class: 'block1-info stext-102 trans-04'}).text(category.descr)
                            ),
                            $('<div/>', {class: 'block1-txt-child2 p-b-4 trans-05'}).append(
                                $('<div/>', {class: 'block1-link stext-101 cl0 trans-09'}).text('Shop Now')
                            )
                        )
                    )
                )
            )
        });
    })
}

function activeCategory(grid) {
    let path = location.search;
    let pathArr = path.split('=');
    let id = +pathArr[1];
    $.getJSON('data/categories.json', function (response) {
        let categories = response.categories;
        categories.forEach((category) => {
            let categoryId = category.categoryId;

            if (categoryId === id) {
                let className = category.categoryName;
                // console.log('name', className);
                grid.isotope({
                    filter: '.' + className
                });

                $('#category .category-item').removeClass('how-active1');
                $(`.filter-tope-group .${className}`).filter(':first').addClass('how-active1');
            }
        });
    });
}

function goToInnerCategory() {
    $(this).on('click', function (event) {
        event.preventDefault();
        let index = event.target.getAttribute('data-id');
        location.href = `product.html?id=${index}`
    })
}