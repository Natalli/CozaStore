$(document).ready(function () {

});
let isValid = false;

function getReviews() {
    let parser = location.search.split('&');
    let artFromURL = parser[1].match(/=\d+/)[0].slice(1);

    $.getJSON('data/reviews.json', response => {
        let allReviews = response.reviews;
        let reviews = allReviews.filter((review) => {
            return (review.art === artFromURL);
        });
        if (localStorage.getItem('reviewsKey')) {
            let allReviewsLoc = JSON.parse(localStorage.getItem('reviewsKey'));
            let reviewsLoc = allReviewsLoc.filter((review) => {
                return (review.art === artFromURL);
            });
            reviews = reviewsLoc;
            allReviews = allReviewsLoc;
        }

        let parent = $('.reviewWrapper');
        forReviews(reviews, parent);
        $('#reviewSubmit').on('click', function () {
            isValid = false;
            let review = $('#addReviewForm textarea[name=review]').val();
            let name = $('#addReviewForm input[name=name]').val();
            let email = $('#addReviewForm input[name=email]').val();
            let date = dateReview();

            checkValidationField(name, email, review);

            if (!isValid) {
                for (let i = 0; i < allReviews.length; i++) {
                    if (allReviews[i].art === artFromURL) {
                        allReviews[i].review.push({name: name, email: email, text: review, date: date});
                        AddStorage(allReviews, 'reviewsKey');
                    }
                }
                cleanReviewWrapper(parent);
                forReviews(reviews, parent);
            }
        });
    });

}

function cleanReviewWrapper(parent) {
    parent.html('');
    $('#reviewCount').html('');
}

function forReviews(reviews, parent) {
    let count = (reviews[0].review.length);
    $('#reviewCount').html('Reviews (' + count + ')');
    reviews[0].review.forEach((review) => {
        $('<div/>', {
            class: 'flex-w flex-t p-b-68',
            html: `<div class="wrap-pic-s size-109 bor0 of-hidden m-r-18 m-t-6">
                            <img src="${review.avatar || 'images/avatar-default.jpg'}" alt="AVATAR">
                        </div>

                        <div class="size-207">
                            <div class="flex-w flex-sb-m p-b-17">
                                    <span class="mtext-107 cl2 p-r-20">
                                        ${review.name}
                                    </span>
                                    <span class="fs-11 cl11">
                                        ${review.date}
                                    </span>
                            </div>

                            <p class="cl6">
                                ${review.text}
                            </p>
                        </div>`
        }).appendTo(parent);
    });
}

function dateReview() {
    let nowDate = new Date();
    let day = nowDate.getDate();
    let month = nowDate.getMonth() + 1;
    let year = nowDate.getFullYear();
    return day + '.' + month + '.' + year
}

function checkValidationField(name, email, review) {
    let patternName = new RegExp(/^[A-Z][a-z]{1,}$/),
        patternEmail = new RegExp(/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/);
    const resultName = patternName.exec(name);
    const resultEmail = patternEmail.exec(email);
    checkForError({key: 'name', pattern: resultName, value: name});
    checkForError({key: 'email', pattern: resultEmail, value: email});

    if (!review) {
        isValid = true;
        return showMessage('Fill the required field', 'review');
    }
}

function checkForError(obj) {
    if (!obj.value) {
        $(`#addReviewForm input[name=${obj.key}]`).addClass('error');
        isValid = true;
        return showMessage('Fill the required field', obj.key);
    } else {
        showMessage('', obj.key);
        $(`#addReviewForm input[name=${obj.key}]`).removeClass('error');
    }

    if (!obj.pattern) {
        $(`#addReviewForm input[name=${obj.key}]`).addClass('error');
        isValid = true;
        return showMessage('Enter valid value', obj.key);
    } else {
        $(`#addReviewForm input[name=${obj.key}]`).removeClass('error');
    }
}

function showMessage(message, thisElem) {
    const element = $(`#addReviewForm input[name=${thisElem}], #addReviewForm textarea[name=${thisElem}]`);
    const elementErrorContainer = $(`#addReviewForm input[name=${thisElem}], #addReviewForm textarea[name=${thisElem}]`).next();
    element.removeClass('error');
    element.addClass('error');
    elementErrorContainer.html('');
    elementErrorContainer.html(message);
}

function AddStorage(obj, key) {
    let elem = JSON.stringify(obj);
    localStorage.setItem(key, elem);
}