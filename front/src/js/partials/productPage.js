'use strict';

$(document).ready(function () {

    $(document).on('click', '.viewProduct', event => {
        event.preventDefault();
        let category = event.target.getAttribute('data-category');
        let art = event.target.getAttribute('data-id');

        location.href = `product-detail.html?category=${category}&art=${art}`;
    });

//------------------------------------------------------------------------
    function showProductDetails() {
        let parser = location.search.split('&');
        let categoryFromURL = parser[0].match(/=\w+/)[0].slice(1);
        let artFromURL = parser[1].match(/=\d+/)[0].slice(1);
        let relatedProducts;

        $.getJSON("data/products.json", response => {
            let selectedProduct = response.products.filter((product) => {
                return (product.art === artFromURL);
            });

            $('.bread-crumb .categoryName').html(`${categoryFromURL} <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>`).attr('href', `product.html?id=${selectedProduct[0].categoryId}`);
            $('.bread-crumb span').html(selectedProduct[0].prod_name);

            $('.productTitle').html(selectedProduct[0].prod_name);
            $('.productPrice').html('$' + selectedProduct[0].cost);

            $('.productDescription').html(selectedProduct[0].descr);
            $('#description p').html(selectedProduct[0].descr);
            $('span.productArt').html(`Product code: ${artFromURL}`);
            $('span.productCategories').html(`Categories: ${categoryFromURL}`);

            getReviews();

            (!selectedProduct[0].size) ? disableSelectOption('.sizeSelect') : generateSelectOptions('.sizeSelect', 'size', selectedProduct[0]);
            (!selectedProduct[0].color) ? disableSelectOption('.colorSelect') : generateSelectOptions('.colorSelect', 'color', selectedProduct[0]);

            let parentImages = $('.sec-product-detail .productImages');

            selectedProduct[0].img.forEach(function (image) {

                $('<div/>', {
                    class: 'item-slick3',
                    'data-thumb': image,
                    html: `<div class="wrap-pic-w pos-relative">
                                                <img src="${image}" alt="IMG-PRODUCT">
                                                <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="${image}">
                                                    <i class="fa fa-expand"></i>
                                                </a>
                                            </div>`

                }).appendTo(parentImages);
            });

            slick3Initial();

            relatedProducts = response.products.filter((product) => {

                return ((product.categoryId === selectedProduct[0].categoryId) && (product.art !== selectedProduct[0].art));
            });

            relatedProducts.forEach((product, index) => {

                $('<div/>', {
                    class: 'item-slick2 p-l-15 p-r-15 p-t-15 p-b-15',
                    html: `<div class="block2">
                            <div class="block2-pic hov-img0">
                             <img src=${product.img[0]} alt="IMG-PRODUCT">
                                <button class="showModal block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04" data-category=${product.categoryId} data-id=${product.art} data-index=${index}>
                                    Quick View
                                </button>
                            </div>
                            <div class="block2-txt flex-w flex-t p-t-14">
                             <div class="block2-txt-child1 flex-col-l">
                              <a href="product-detail.html?category=${categoryFromURL}&art=${product.art}" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                        ${product.prod_name}
                                    </a>
                                    <span class="stext-105 cl3">
										$ ${product.cost}
									</span>
                             </div>
                            </div>
                           </div>`
                }).appendTo($('.slick2'));
            });

            slickTwoInitial();

        });

        //-----------------------Modal--------------------------------
        $('.wrap-slick2').on('click', '.showModal', e => {

            $('#modal .sizeSelect option:not(:first-child)').remove();

            $('#modal .colorSelect option:not(:first-child)').remove();
            $('#modal .productImages').html('');

            let index = e.target.getAttribute('data-index');
            let modal = $('#modal');
            modal
                .modal();

            $('#modal .productName').html(relatedProducts[index].prod_name);
            $('#modal .productCost').html('$' + relatedProducts[index].cost);
            $('#modal .productDescription').html(relatedProducts[index].descr);


            (!relatedProducts[index].size) ? disableSelectOption('#modal .sizeSelect') : generateSelectOptions('#modal .sizeSelect', 'size', relatedProducts[index]);
            (!relatedProducts[index].color) ? disableSelectOption('#modal .colorSelect') : generateSelectOptions('#modal .colorSelect', 'color', relatedProducts[index]);

            let parentImagesForModal = $('#modal .productImages');
            console.log('1111', relatedProducts[index].img[0]);
            $('<div/>', {
                class: 'item',
                html: `<img src="${relatedProducts[index].img[0]}" alt="IMG-PRODUCT">
                               <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="${relatedProducts[index].img[0]}">
                                   <i class="fa fa-expand"></i>
                               </a>`
            }).appendTo(parentImagesForModal);
        });

        //-------------------------------------------------------------

    }

    (location.pathname === '/product-detail.html') && showProductDetails();


    function disableSelectOption(selectElement) {
        $(selectElement).attr('disabled', 'true');
        $(`${selectElement} + span span[title="Choose an option"]`).html('option is not available');
    }

    function generateSelectOptions(selectElement, option, product) {
        let parent = $(selectElement);
        product[option].forEach(function (option) {
            $('<option/>', {
                html: option
            }).appendTo(parent);
        });
    }

    function slick3Initial() {
        $('.sec-product-detail .wrap-slick3').each(function () {
            $(this).find('.slick3').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                fade: true,
                infinite: true,
                autoplay: false,
                autoplaySpeed: 6000,

                arrows: true,
                appendArrows: $(this).find('.wrap-slick3-arrows'),
                prevArrow: '<button class="arrow-slick3 prev-slick3"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
                nextArrow: '<button class="arrow-slick3 next-slick3"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',

                dots: true,
                appendDots: $(this).find('.wrap-slick3-dots'),
                dotsClass: 'slick3-dots',
                customPaging: function (slick, index) {
                    let portrait = $(slick.$slides[index]).data('thumb');
                    console.log('slick3', index);
                    return '<img src=" ' + portrait + ' "/><div class="slick3-dot-overlay"></div>';
                },

            });
        });
    }

    function slickTwoInitial() {
        $('.wrap-slick2').each(function () {
            $(this).find('.slick2').slick({
                slidesToShow: 4,
                slidesToScroll: 4,
                infinite: false,
                autoplay: false,
                autoplaySpeed: 6000,
                arrows: true,
                appendArrows: $(this),
                prevArrow: '<button class="arrow-slick2 prev-slick2"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
                nextArrow: '<button class="arrow-slick2 next-slick2"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 4
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 576,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        });


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            let nameTab = $(e.target).attr('href');
            $(nameTab).find('.slick2').slick('reinit');
        });
    }



});


